{
#include<bits/stdc++.h>
using namespace std;
vector<int> graph[10001];
int * topoSort(vector<int> graph[],int N);
int main()
{
int T;
cin>>T;
while(T--)
{
    memset(graph,0,sizeof graph);
    int N,E;
    cin>>E>>N;
    for(int i=0;i<E;i++)
    {
        int u,v;
        cin>>u>>v;
        graph[u].push_back(v);
    }
    int *res = topoSort(graph,N);
    bool valid =true;
    for(int i=0;i<N;i++)
    {
        int n=graph[res[i]].size();
        for(int j=0;j<graph[res[i]].size();j++)
        {
            for(int k=i+1;k<N;k++)
            {
                if(res[k]==graph[res[i] ] [j] )
                    n--;
            }
        }
        if(n!=0)
        {
            valid =false;
            break;
        }
    }
    if(valid==false)
        cout<<0<<endl;
    else
        cout<<1<<endl;
}
}

}

  int tim[100] , num =0 , vis[100];
     int re[100];
  int DFS(vector<int> graph[] , int x){
      vis[x]=true;
      for(int i = 0 ; i < graph[x].size();i++) 
      if(!vis[graph[x][i]]) DFS(graph,graph[x][i]);
      tim[x] = num++;
  }
int * topoSort(vector<int> graph[], int N)
{
        memset(vis , false , sizeof(vis));
        for(int i =0 ; i <N;i++)
        if(!vis[i])
        DFS(graph , i);
        vector<pair<int , int> > p;
                p.clear();

        for(int i =0 ; i<N;i++) p.push_back(make_pair(tim[i] ,i));
        sort(p.begin() ,p.end() , greater<pair<int , int> >());
     
        for(int i= 0; i<N;i++) re[i]= p[i].second;
        return re;
  
}